package com.zyy.config;

import com.zyy.constants.ConstantToRabbitMQ;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: 类描述
 * @Author: zyy
 * @Date: 2023/05/25 23:51
 */
@Configuration
public class FanoutRabbitConfig {
    /**
     * 定义交换机
     */
    @Bean("fanoutExchange")
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange(ConstantToRabbitMQ.EXCHANGE_NAME, true, false);
    }


    /**
     * 队列绑定交换机
     */
    @Bean
    public Binding emailBinding(@Qualifier("emailQueue") Queue queue, @Qualifier("fanoutExchange") FanoutExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange);
    }

    @Bean
    public Binding smsBinding(@Qualifier("smsQueue") Queue queue, @Qualifier("fanoutExchange") FanoutExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange);
    }

    @Bean
    public Binding weixinBinding(@Qualifier("weixinQueue") Queue queue, @Qualifier("fanoutExchange") FanoutExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange);
    }
}
