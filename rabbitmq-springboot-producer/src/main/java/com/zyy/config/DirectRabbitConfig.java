package com.zyy.config;

import com.zyy.constants.ConstantToRabbitMQ;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: 类描述
 * @Author: zyy
 * @Date: 2023/05/25 23:51
 */
@Configuration
public class DirectRabbitConfig {
    /**
     * 定义交换机
     */
    @Bean("directExchange")
    public DirectExchange directExchange() {
        return new DirectExchange(ConstantToRabbitMQ.EXCHANGE_NAME_DIRECT, true, false);
    }

    @Bean("deadExchange")
    public DirectExchange deadExchange() {
        return new DirectExchange(ConstantToRabbitMQ.EXCHANGE_NAME_DEAD_DIRECT, true, false);
    }

    /**
     * 定义队列，这里不再重复定义队列，就用
     */

    /**
     * 队列绑定交换机
     */
    @Bean
    public Binding emailDirectBinding(@Qualifier("emailQueue") Queue queue, @Qualifier("directExchange") DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ConstantToRabbitMQ.ROUTING_KEY_EMAIL);
    }

    @Bean
    public Binding smsDirectBinding(@Qualifier("smsQueue") Queue queue, @Qualifier("directExchange") DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ConstantToRabbitMQ.ROUTING_KEY_SMS);
    }

    @Bean
    public Binding weixinDirectBinding(@Qualifier("weixinQueue") Queue queue, @Qualifier("directExchange") DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ConstantToRabbitMQ.ROUTING_KEY_WEIXIN);
    }

    @Bean
    public Binding ttlDirectBinding(@Qualifier("ttlQueue") Queue queue, @Qualifier("directExchange") DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ConstantToRabbitMQ.ROUTING_KEY_TTL);
    }


    @Bean
    public Binding deaDBinding(@Qualifier("deadQueue") Queue queue, @Qualifier("deadExchange") DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ConstantToRabbitMQ.ROUTING_KEY_DEAD);
    }
}
