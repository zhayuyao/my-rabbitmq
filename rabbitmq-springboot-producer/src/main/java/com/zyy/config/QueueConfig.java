package com.zyy.config;

import com.zyy.constants.ConstantToRabbitMQ;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 类描述
 * @Author: zyy
 * @Date: 2023/05/28 14:52
 */
@Configuration
public class QueueConfig {
    /**
     * 定义队列：邮件队列
     */
    @Bean("emailQueue")
    public Queue emailQueue() {
        return new Queue(ConstantToRabbitMQ.QUEUE_NAME_EMAIL, true);
    }

    /**
     * 定义队列：短信队列
     */
    @Bean("smsQueue")
    public Queue smsQueue() {
        return new Queue(ConstantToRabbitMQ.QUEUE_NAME_SMS, true);
    }

    /**
     * 定义队列：微信队列
     */
    @Bean("weixinQueue")
    public Queue weixinQueue() {
        return new Queue(ConstantToRabbitMQ.QUEUE_NAME_WEIXIN, true);
    }


    @Bean("ttlQueue")
    public Queue ttlQueue() {
        //消息5秒后自动删除
        Map<String, Object> argMap = new HashMap<>();
        //过期时间，单位毫秒数
        argMap.put("x-message-ttl", 5000);
        //队列的最大长度
        argMap.put("x-max-length", 5);
        //死信队列
        argMap.put("x-dead-letter-exchange", ConstantToRabbitMQ.EXCHANGE_NAME_DEAD_DIRECT);
        //死信队列 的 routing-key
        argMap.put("x-dead-letter-routing-key", ConstantToRabbitMQ.ROUTING_KEY_DEAD);
        //public Queue(String name, boolean durable, boolean exclusive, boolean autoDelete, @Nullable Map<String, Object> arguments) {
        return new Queue(ConstantToRabbitMQ.QUEUE_NAME_TTL, true, false, false, argMap);
    }


    @Bean("deadQueue")
    public Queue deadQueue() {
        return new Queue(ConstantToRabbitMQ.QUEUE_NAME_DEAD, true);
    }

}
