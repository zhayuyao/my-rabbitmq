package com.zyy.config;

import com.zyy.constants.ConstantToRabbitMQ;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: 类描述
 * @Author: zyy
 * @Date: 2023/05/25 23:51
 */
@Configuration
public class TopicRabbitConfig {
    /**
     * 定义交换机
     */
    @Bean("topicExchange")
    public TopicExchange directExchange() {
        return new TopicExchange(ConstantToRabbitMQ.EXCHANGE_NAME_TOPIC, true, false);
    }

    /**
     * 定义队列，这里不再重复定义队列，就用
     */

    /**
     * 队列绑定交换机
     */
    @Bean
    public Binding emailTopicBinding(@Qualifier("emailQueue") Queue queue, @Qualifier("topicExchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("*.email.*");
    }

    @Bean
    public Binding smsTopicBinding(@Qualifier("smsQueue") Queue queue, @Qualifier("topicExchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("#.sms.#");
    }

    @Bean
    public Binding weixinTopicBinding(@Qualifier("weixinQueue") Queue queue, @Qualifier("topicExchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("weixin.#");
    }
}
