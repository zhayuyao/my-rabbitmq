package com.zyy.consumer;

import com.zyy.constants.ConstantToRabbitMQ;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @Description: 类描述
 * @Author: zyy
 * @Date: 2023/05/26 00:10
 */
@Service
public class OrderService {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void makeOrder(Long userId, Long productId, int num) {
        String orderNumber = UUID.randomUUID().toString();

        System.out.println("用户：" + userId + "，订单编码是：" + orderNumber);
        rabbitTemplate.convertAndSend(ConstantToRabbitMQ.EXCHANGE_NAME, "", orderNumber);
    }

    public void makeDirectOrder(Long userId, Long productId, int num) {
        String orderNumber = UUID.randomUUID().toString();

        System.out.println("用户：" + userId + "，订单编码是：" + orderNumber);

        /*MessagePostProcessor messagePostProcessor = message -> {
            MessageProperties messageProperties = message.getMessageProperties();
            messageProperties.setExpiration("5000");
            return message;
        };*/

        rabbitTemplate.convertAndSend(ConstantToRabbitMQ.EXCHANGE_NAME_DIRECT, ConstantToRabbitMQ.ROUTING_KEY_TTL, orderNumber);
//        rabbitTemplate.convertAndSend(ConstantToRabbitMQ.EXCHANGE_NAME_DIRECT, ConstantToRabbitMQ.ROUTING_KEY_SMS, orderNumber, messagePostProcessor);
//        rabbitTemplate.convertAndSend(ConstantToRabbitMQ.EXCHANGE_NAME_DIRECT, ConstantToRabbitMQ.ROUTING_KEY_WEIXIN, orderNumber);
    }

    public void makeDirectTopic(Long userId, Long productId, int num) {
        String orderNumber = UUID.randomUUID().toString();

        System.out.println("用户：" + userId + "，订单编码是：" + orderNumber);

//        String routingKey = "sms.email.test";
        String routingKey = "weixin.email.test";

        rabbitTemplate.convertAndSend(ConstantToRabbitMQ.EXCHANGE_NAME_TOPIC, routingKey, orderNumber);
    }
}
