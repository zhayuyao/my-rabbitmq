package com.zyy;

import com.zyy.consumer.OrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RabbitmqSpringbootProducerApplicationTests {

    @Autowired
    private OrderService orderService;

    @Test
    void contextLoads() {
        orderService.makeOrder(1L, 1L, 10);
    }


    @Test
    void testDirect() {
        for (int i = 0; i < 11; i++) {
            orderService.makeDirectOrder(2L, 2L, 10);
        }
    }

    @Test
    void testTopic() {
        orderService.makeDirectTopic(3L, 3L, 10);
    }

}
