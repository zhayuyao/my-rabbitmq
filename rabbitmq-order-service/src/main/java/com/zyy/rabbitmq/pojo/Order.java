package com.zyy.rabbitmq.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 类描述
 * @Author: zyy
 * @Date: 2023/06/03 16:59
 */
@Data
public class Order implements Serializable {
    private static final long serialVersionUID = 2231174433174810752L;

    public Integer orderId;
    public Integer userId;
    public String orderContent;
    public Date createTime;

}
