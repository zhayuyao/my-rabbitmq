package com.zyy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RabbitmqOrderServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitmqOrderServiceApplication.class, args);
    }

}
