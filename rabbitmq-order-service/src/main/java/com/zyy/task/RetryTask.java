package com.zyy.task;

import com.alibaba.fastjson2.JSON;
import com.zyy.rabbitmq.pojo.Order;
import com.zyy.rabbitmq.service.MQOrderService;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Description: 类描述
 * @Author: zyy
 * @Date: 2023/06/13 23:29
 */
@Component
public class RetryTask {
    @Resource
    private JdbcTemplate jdbcTemplate;
    @Resource
    private MQOrderService mqOrderService;

    @Scheduled(cron = "*/10 * * * * ?")
    public void retry() {
        //查询发送失败的任务  TODO 注意：这里应该需要控制重试次数，不能无限制重试
        String sql = "SELECT order_id 'orderId',user_id 'userId',order_content 'orderContent',`status`,create_time 'createTime' FROM order_message WHERE STATUS=2";

        List<Map<String, Object>> taskList = jdbcTemplate.queryForList(sql);
        if (taskList == null || taskList.size() == 0) {
            return;
        }
        List<Order> orderList = JSON.parseArray(JSON.toJSONString(taskList), Order.class);
        for (Order order : orderList) {
            //mq推送给运单
            mqOrderService.sendMessage(order);
        }
    }
}
