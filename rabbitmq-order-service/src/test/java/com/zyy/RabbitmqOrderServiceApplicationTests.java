package com.zyy;

import com.zyy.rabbitmq.pojo.Order;
import com.zyy.rabbitmq.service.OrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;


@SpringBootTest
class RabbitmqOrderServiceApplicationTests {


    @Autowired
    private OrderService orderService;

    @Test
    void orderCreate() {
        Order order = new Order();
        order.setUserId(1);
        order.setOrderId(8);
        order.setOrderContent("重庆鸡公煲");
        order.setCreateTime(new Date());

        try {
            orderService.createOrder(order);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    void orderCreateMessage() {
        Order order = new Order();
        order.setUserId(1);
        order.setOrderId(1006);
        order.setOrderContent("重庆鸡公煲");
        order.setCreateTime(new Date());

        try {
            orderService.createOrderMessage(order);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
