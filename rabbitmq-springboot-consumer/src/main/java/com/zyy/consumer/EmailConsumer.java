package com.zyy.consumer;

import com.zyy.constants.ConstantToRabbitMQ;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Description: 类描述
 * @Author: zyy
 * @Date: 2023/05/28 12:15
 */
@Component
public class EmailConsumer {
    @RabbitListener(queues = ConstantToRabbitMQ.QUEUE_NAME_EMAIL)
    public void messageReceive(String message) {
        System.out.println("email------------->" + message);
    }
}
