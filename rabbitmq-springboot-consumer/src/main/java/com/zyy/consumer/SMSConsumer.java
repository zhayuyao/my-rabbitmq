package com.zyy.consumer;

import com.zyy.constants.ConstantToRabbitMQ;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;


@Component
public class SMSConsumer {
    @RabbitListener(queues = ConstantToRabbitMQ.QUEUE_NAME_SMS)
    public void messageReceive(String message) {
        System.out.println("sms------------->" + message);
    }
}
