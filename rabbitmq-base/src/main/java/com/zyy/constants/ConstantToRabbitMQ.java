package com.zyy.constants;

/**
 * @Description: 类描述
 * @Author: zyy
 * @Date: 2023/05/28 15:11
 */
public class ConstantToRabbitMQ {

    public static final String EXCHANGE_NAME = "fanout_order_exchange";
    public static final String EXCHANGE_NAME_DIRECT = "direct_order_exchange";
    public static final String EXCHANGE_NAME_DEAD_DIRECT = "dead_direct_order_exchange";
    public static final String EXCHANGE_NAME_TOPIC = "topic_order_exchange";

    public static final String ROUTING_KEY_SMS = "sms";
    public static final String ROUTING_KEY_EMAIL = "email";
    public static final String ROUTING_KEY_WEIXIN = "weixin";
    public static final String ROUTING_KEY_TTL = "ttl";
    public static final String ROUTING_KEY_DEAD = "dead";


    public static final String QUEUE_NAME_SMS = "sms.queue";
    public static final String QUEUE_NAME_EMAIL = "email.queue";
    public static final String QUEUE_NAME_WEIXIN = "weixin.queue";
    public static final String QUEUE_NAME_DEAD = "dead.queue";
    public static final String QUEUE_NAME_TTL = "ttl.queue";
}
