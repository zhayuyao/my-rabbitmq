package com.zyy.rabbitmq.web;

import com.zyy.rabbitmq.service.DispatcherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 类描述
 * @Author: zyy
 * @Date: 2023/06/03 22:37
 */
@RestController
public class DispatcherController {
    @Autowired
    DispatcherService dispatcherService;

    /**
     * 创建配送单
     *
     * @param orderId
     */
    @GetMapping("/dispatcher/create")
    public String createDispatcher(@RequestParam Integer orderId) {
        try {
            if (orderId.equals(8)) {
                //模拟耗时，接口调用者会认为超时
                Thread.sleep(4000L);
            }
            dispatcherService.createDispatcher(orderId);
            return "success";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";

    }
}
