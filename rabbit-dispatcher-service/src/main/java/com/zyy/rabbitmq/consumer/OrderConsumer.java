package com.zyy.rabbitmq.consumer;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.rabbitmq.client.Channel;
import com.zyy.rabbitmq.service.DispatcherService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @Description: 类描述
 * @Author: zyy
 * @Date: 2023/06/12 21:45
 */
@Component
public class OrderConsumer {

    @Autowired
    private DispatcherService dispatcherService;

    @RabbitListener(queues = "order_dispatcher")
    public void messageReceive(String message, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) {
        try {
            JSONObject messageJSON = JSON.parseObject(message);
            Integer orderId = messageJSON.getInteger("orderId");
            System.out.println(1 / 0); //这里异常，消息会一直重发
            dispatcherService.createDispatcher(orderId);

            channel.basicAck(tag, false);

            /**
             * 解决这个问题
             * 1.控制重发次数
             * 2.try+catch+手动ack + 死信队列 + 人工干预
             */
        } catch (Exception e) {
            /**
             * 如果出现异常的情况，根据实际的情况去重发
             * 重发一次后，丢失，还是存库根据自己的业务场景去决定
             * long deliveryTag, 消息tag
             * boolean multiple, 多条处理
             * boolean requeue  重发
             *   false 不会重发，会把消息打入死信队列
             *   true 会死循环的重发，如果使用true 建议不用加try/catch 否则会造成死循环
             */
            try {
                channel.basicNack(tag, false, false);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

}
