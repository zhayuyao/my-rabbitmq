package com.zyy.rabbitmq.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @Description: 类描述
 * @Author: zyy
 * @Date: 2023/06/03 21:51
 */
@Service
public class DispatcherService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional(rollbackFor = Exception.class)
    public void createDispatcher(int orderId) throws Exception {

        String sql = "insert into dispatcher(order_id, status, create_time) values(?,?,?)";

        Date date = new Date();
        int count = jdbcTemplate.update(sql, orderId, 0, date);
        if (count != 1) {
            throw new Exception("配送单创建失败！");
        }
    }
}
