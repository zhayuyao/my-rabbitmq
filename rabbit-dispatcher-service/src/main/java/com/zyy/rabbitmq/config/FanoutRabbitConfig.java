package com.zyy.rabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: 类描述
 * @Author: zyy
 * @Date: 2023/06/12 22:46
 */
@Configuration
public class FanoutRabbitConfig {

    @Bean("fanoutExchange")
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange("order.fanout.exchange", true, false);
    }

    @Bean("deadExchange")
    public FanoutExchange deadFanoutExchange() {
        return new FanoutExchange("dead.order.fanout.exchange", true, false);
    }


    @Bean
    public Binding orderBinding(@Qualifier("orderQueue") Queue queue, @Qualifier("fanoutExchange") FanoutExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange);
    }

    @Bean
    public Binding deadOrderBinding(@Qualifier("deadQueue") Queue queue, @Qualifier("deadExchange") FanoutExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange);
    }
}
