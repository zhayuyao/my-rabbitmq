package com.zyy.rabbitmq.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 类描述
 * @Author: zyy
 * @Date: 2023/06/12 22:36
 */
@Configuration
public class QueueConfig {

    @Bean("orderQueue")
    public Queue orderQueue() {
        //消息5秒后自动删除
        Map<String, Object> argMap = new HashMap<>();
        //死信队列
        argMap.put("x-dead-letter-exchange", "dead.order.fanout.exchange");
        return new Queue("order_dispatcher", true, false, false, argMap);
    }

    @Bean("deadQueue")
    public Queue deadQueue() {
        return new Queue("dead_order_dispatcher", true);
    }
}
