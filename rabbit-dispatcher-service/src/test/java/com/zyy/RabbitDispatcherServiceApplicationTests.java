package com.zyy;

import com.zyy.rabbitmq.service.DispatcherService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RabbitDispatcherServiceApplicationTests {

    @Autowired
    DispatcherService dispatcherService;
    @Test
    void contextLoads() {
        try {
            dispatcherService.createDispatcher(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
