package com.zyy.rabbitmq.simple;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @Description: 类描述
 * @Author: zyy
 * @Date: 2023/05/18 22:56
 */
public class Producer {
    public static void main(String[] args) {
        //1.创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        //2.设置连接属性
        connectionFactory.setHost("39.108.49.252");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("admin");
        connectionFactory.setPassword("admin");
        connectionFactory.setVirtualHost("/");
        Connection connection = null;
        Channel channel = null;
        try {
            //3.从连接工厂中获取连接
            connection = connectionFactory.newConnection("zyy_producer");
            //4.从连接中创建通道channel
            channel = connection.createChannel();
            /**
             * 5.声明队列queue
             * @param queue 队列的名称
             * @param durable 队列是否持久化
             * @param exclusive 是否排他，即是否私有化，如果为true，会对当前队列加锁，其他的通道不能访问，并且连接自动关闭
             * @param autoDelete 是否自动删除，当最后一个消费者断开连接之后是否自动删除消息
             * @param arguments 可以设置队列的附件参数，设置队列的有效期，消息的最大长度，队列的消息生命周期等
             */
            channel.queueDeclare("zyy_queue", false, false, false, null);
            //6.发送消息
            String message = "zyy hello!";
            /**
             *
             * @param exchange 交换机exchange
             * @param routingKey 队列对称
             * @param props 属性配置
             * @param body 发送消息的内容
             */
            channel.basicPublish("", "zyy_queue", null, message.getBytes());
            System.out.println("消息发送成功");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } finally {
            if (channel != null && channel.isOpen()) {
                try {
                    channel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
            //7.释放连接
            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }


    }
}
